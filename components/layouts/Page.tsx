import React from "react";
import Head from "next/head";

interface IProps {
  children: React.ReactNode;
}

const Page = ({ children }: IProps) => {
  return (
    <>
      <Head>
        <title>Multipage form</title>
      </Head>
      <main>{children}</main>
    </>
  );
};

export default Page;
