import {
  Container,
  Tabs,
  TabList,
  Tab,
  TabPanels,
  TabPanel,
  Heading,
} from "@chakra-ui/react";
import React from "react";
import InterviewSettings from "../InterviewDetails/InterviewSettings";
import JobDetails from "../JobDetails/JobDetails";
import RequistionDetails from "../Requisiton/RequistionDetails";
import DisplayCard from "../DisplayCard";
import { useSelector } from "react-redux";

interface IProps {
  title: String;
}

function CustomTab({ title }: IProps) {
  return (
    <Tab
      p={{
        base: "1rem",
      }}
      fontFamily="Poppins"
    >
      {title}
    </Tab>
  );
}

const TabLayout = () => {
  const count = useSelector((state: any) => {
    console.log(state);
    return state.count.count;
  });

  return (
    <Container
      display="grid"
      maxW={{ base: "80vw" }}
      gridTemplateColumns="1fr 0.7fr"
      gap={10}
    >
      <Tabs index={count}>
        <Heading fontFamily="Poppins" fontSize="1.5rem" mt="2rem" mb="1rem">
          Create Candidate Requisition
        </Heading>
        <TabList>
          <CustomTab title="Requistion Details" />
          <CustomTab title="Job Details" />
          <CustomTab title="Interview Settings" />
        </TabList>
        <TabPanels>
          <TabPanel>
            <RequistionDetails />
          </TabPanel>
          <TabPanel>
            <JobDetails />
          </TabPanel>
          <TabPanel>
            <InterviewSettings />
          </TabPanel>
        </TabPanels>
      </Tabs>
      <DisplayCard />
    </Container>
  );
};

export default TabLayout;
