import {
  FormLabel,
  Box,
  FormErrorMessage,
  FormErrorIcon,
  FormControl,
} from "@chakra-ui/react";
import React from "react";

interface IProps {
  label: String;
}

interface IElementProps {
  children: React.ReactNode;
  isInvalid: any;
}

function CustomFormLabel({ label }: IProps) {
  return (
    <FormLabel fontFamily="Poppins" color="gray" fontSize="0.8rem">
      {label}
    </FormLabel>
  );
}

function FormElement({ children, isInvalid }: IElementProps) {
  return (
    <Box my="1rem">
      <FormControl>{children}</FormControl>
    </Box>
  );
}

export { CustomFormLabel, FormElement };
