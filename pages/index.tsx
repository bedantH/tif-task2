import type { NextPage } from "next";
import TabLayout from "../components/layouts/TabLayout";
import Page from "../components/layouts/Page";


const Home: NextPage = () => {
  return (
    <Page>
      <TabLayout />
    </Page>
  );
};

export default Home;
