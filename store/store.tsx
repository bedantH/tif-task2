import { configureStore } from "@reduxjs/toolkit";
import counterSlice from "./counterSlice";
import formSlice from "./formSlice";

const store = configureStore({
  reducer: {
    form: formSlice,
    count: counterSlice,
  },
});

export default store;
