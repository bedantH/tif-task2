const { createSlice } = require("@reduxjs/toolkit");

const counterSlice = createSlice({
  name: "counter",
  initialState: {
    count: 0,
  },
  reducers: {
    increment: (state: any) => {
      console.log({ state });
      if (state.count < 2) {
        state.count = state.count + 1;
      }
    },
    decrement: (state: any) => {
      if (state.count >= 0) {
        state.count = state.count - 1;
      }
    },
  },
});

export const { increment, decrement } = counterSlice.actions;
export default counterSlice.reducer;
